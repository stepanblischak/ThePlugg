#ifndef PLUGG_H
#define PLUGG_H

#include "arm.h"

#include <iostream>
#include <functional>
#include <algorithm>
#include <map>

// This class has information about arms
// and do some calculations using it
class Plugg
{
public:
    // input values type
    // to specify by variable of this type each input
    enum inputs_t {LEFT, RIGHT, TOTAL};

    // constructors
    Plugg(float coeff1, float bet1, float coeff2, float bet2);
    Plugg(const Arm& left, const Arm& right);

    // function to calculate bets using ratio and fixed values
    void calculate_bets();
    std::pair<float, float> calculate_result();
    std::pair<float, float> rebase_calculations();
    std::pair<float, float> rebase_calculations(float coeff1, float coeff2);

    // getters for left and right arm and bet
    float getBet(inputs_t bet) const;
    void setBet(inputs_t bet, float val);
    float getCoeff(inputs_t bet) const;
    void setCoeff(inputs_t bet, float val);
    // function gets fixed_t type value and due to it
    // set the fixed value
    inputs_t getCurrentlyFixed() const;
    void setCurrentlyFixed(inputs_t value);
    // for distributive parametr
    inputs_t getDistributive() const;
    void setDistributive(inputs_t value);
    // get profit of this plugg
    float getProfit() const;

    // for formated output
    friend std::ostream &operator<< (std::ostream &os, Plugg const &plugg);

protected:

    /// TODO: static or members ????

    // link values using fixed value and linkTo value
    void linkInputs(inputs_t linkTo);
    // link value using fixed value
    void linkLeft();
    void linkRight();
    void linkTotal();

    // evaluate functions for each linking
    void evalLeft();
    void evalRight();
    void evalTotal();

    // evaluating ratio
    static float distributiveLeft(const Plugg& plugg) { return plugg.right.getCoeff() - 1; }
    static float distributiveRight(const Plugg& plugg) { return 1 / (plugg.left.getCoeff() - 1); }
    static float distributiveTotal(const Plugg& plugg) { return plugg.right.getCoeff() / plugg.left.getCoeff(); }

protected:
    // left and right arms of a plugg
    Arm left, right;
    // a total sum of bets in left and right arms
    FixedValue<float> total;

    // currently fixed value (value to keep fixed if calculations)
    inputs_t currentlyFixed = LEFT;
    // distribute type
    inputs_t distributive;

    // a ratio between left and right arm
    float ratio;

    // a translation map [input] -> object pointer
    std::map<inputs_t, FixedValue<float>*> translation =
    {
            {LEFT, &left},
            {RIGHT, &right},
            {TOTAL, &total}
    };
    // a translation only for Arm objects
    std::map<inputs_t, Arm*> arms =
    {
            {LEFT, &left},
            {RIGHT, &right}
    };
    // declaring an object of a function type
    std::map<inputs_t, std::function<float(const Plugg&)>> ratio_evaluators =
    {
        {LEFT, distributiveLeft},
        {RIGHT, distributiveRight},
        {TOTAL, distributiveTotal}
    };
};

#endif // PLUGG_H
