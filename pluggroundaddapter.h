#ifndef PLUGGROUNDADDAPTER_H
#define PLUGGROUNDADDAPTER_H

#include "plugg.h"

// A class that inherits the Plugg class
// to make some rounding on bets
class PluggRounded : public Plugg
{
public:
    // constructors
    PluggRounded(float coeff1, float bet1, float coeff2, float bet2, int rounding_=defaultRounding);
    PluggRounded(const Arm& left, const Arm& right, int rounding_=defaultRounding);
    // overwritten
    void setBet(inputs_t bet, float val);
    void setCoeff(inputs_t bet, float val);
    // getter and setter for rounding
    int getRounding() const;
    void setRounding(int value);

private:
    // rounding: rounds all but not fixed and inputing
    void roundIt(inputs_t inputing);
    // rounding: rounds all
    void roundIt();

public:

    // rounding by default
    static const int defaultRounding = 1;

private:
    // rounding value
    int rounding;
};

#endif // PLUGGROUNDADDAPTER_H
