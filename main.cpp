#include "pluggwindow.h"
#include <QApplication>

#ifdef QT_DEBUG
#include <iostream>
#include <QDebug>
#endif

#include "plugg.h"
#include "pluggroundaddapter.h"

#ifdef QT_DEBUG
void test()
{
    PluggRounded plugg(Arm(3.2f, 0.0f), Arm(1.78f, 0.0f));
    plugg.setCurrentlyFixed(Plugg::inputs_t::LEFT);
    plugg.setDistributive(Plugg::inputs_t::TOTAL);

    plugg.setCoeff(Plugg::inputs_t::TOTAL, 3.2);
    qDebug() << "Get coeff" << plugg.getCoeff(Plugg::inputs_t::TOTAL);
    qDebug() << "Fixed" << plugg.getCurrentlyFixed();
    plugg.setBet(Plugg::inputs_t::LEFT, 100.0f);
    plugg.setBet(Plugg::inputs_t::RIGHT, 100.0f);
    plugg.setCurrentlyFixed(Plugg::inputs_t::RIGHT);
    auto res = plugg.calculate_result();
//    auto corr = plugg.rebase_calculations();
//    std::cout << plugg << std::endl;
//    std::cout << res.first << " " << res.second << std::endl;
//    std::cout << corr.first << " " << corr.second << std::endl;
}
 #endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PluggWindow w;
    w.show();

 #ifdef QT_DEBUG
    test();
#endif

    return a.exec();
}
