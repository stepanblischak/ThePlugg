#ifndef ARM_H
#define ARM_H

#include "fixedvalue.h"

// a class with bet and coefficient of event
// inherits FixedValue to have this fixed flag
// needed by the application logic
class Arm : public FixedValue<float>
{
public:
    Arm(float coeff, float bet = 0, bool fixed = false);
    Arm(const Arm& another_arm);

    // bet value for this arm
    float getBet() const;
    void setBet(float value);

    // coefficient for this arm
    float getCoeff() const;
    void setCoeff(float value);

private:
    float coeff;
};

#endif // ARM_H
