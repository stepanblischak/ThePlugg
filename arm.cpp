#include "arm.h"

#include <stdexcept>

Arm::Arm(float coeff, float bet, bool fixed)
{
    // init our class with coeff and bet
    setCoeff(coeff);
    setBet(bet);
    setFixed(fixed);
}

Arm::Arm(const Arm &another_arm) : FixedValue(another_arm)
{
    coeff = another_arm.coeff;
}

float Arm::getBet() const
{
    return getValue();
}

void Arm::setBet(float value)
{
    // check for positive value
    if (value < 0)
    {
        // if negative throw an exception
        throw std::invalid_argument( "received negative value" );
    }
    // set the value
    setValue(value);
}

float Arm::getCoeff() const
{
    return coeff;
}

void Arm::setCoeff(float value)
{
    // check for positive value
    if (value < 0)
    {
        // if negative throw an exception
        throw std::invalid_argument("received negative value");
    }
    else if (value == 0)
    {
        // if zero throw an exception
        throw std::invalid_argument("received zero value for coefficient");
    }
    // set the value
    coeff = value;
}
