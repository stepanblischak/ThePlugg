#include "pluggwindow.h"
#include "ui_mainwindow.h"
#include "utils.h"

#ifdef QT_DEBUG
#include <QDebug>
#include <iostream>
#endif

PluggWindow::PluggWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PluggMainWindow),
    plugg(Arm(1.0f, 0.0f), Arm(1.0f, 0.0f))
{
    ui->setupUi(this);
    setWindowTitle(windowName);
    setWindowIcon(QIcon("plugg.ico"));
    setupWidgets();
    plugg.setCurrentlyFixed(Plugg::inputs_t::LEFT);
    plugg.setDistributive(Plugg::inputs_t::TOTAL);
    updateInputs();
}

PluggWindow::~PluggWindow()
{
    delete ui;
}

void PluggWindow::setupWidgets()
{
    for (const int roundValue: roundingValues)
    {
        ui->roundingbox->addItem(QString::number(roundValue));
    }

    // TODO:
    roundingChanged(0);

    // TODO: Refactor this
    ui->fixed1->setChecked(true);
    ui->distr1->setChecked(true);
    ui->distr2->setChecked(true);
    setFixed();

    connect(ui->roundingbox, SIGNAL(currentIndexChanged(int)), this, SLOT(roundingChanged(int)));

    connect(ui->roundingbox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateInputs()));

    connect(ui->fixed1, SIGNAL(clicked(bool)), this, SLOT(setFixed()));
    connect(ui->fixed2, SIGNAL(clicked(bool)), this, SLOT(setFixed()));
    connect(ui->fixed12, SIGNAL(clicked(bool)), this, SLOT(setFixed()));

    connect(ui->fixed1, SIGNAL(clicked(bool)), this, SLOT(updateInputs()));
    connect(ui->fixed2, SIGNAL(clicked(bool)), this, SLOT(updateInputs()));
    connect(ui->fixed12, SIGNAL(clicked(bool)), this, SLOT(updateInputs()));

    connect(ui->distr1, SIGNAL(clicked(bool)), this, SLOT(setDistributive()));
    connect(ui->distr2, SIGNAL(clicked(bool)), this, SLOT(setDistributive()));

    connect(ui->distr1, SIGNAL(clicked(bool)), this, SLOT(updateInputs()));
    connect(ui->distr2, SIGNAL(clicked(bool)), this, SLOT(updateInputs()));

    connect(ui->am1coeff, SIGNAL(textEdited(QString)), this, SLOT(updateInputs()));
    connect(ui->am2coeff, SIGNAL(textEdited(QString)), this, SLOT(updateInputs()));
    connect(ui->bet1, SIGNAL(textEdited(QString)), this, SLOT(updateInputs()));
    connect(ui->bet2, SIGNAL(textEdited(QString)), this, SLOT(updateInputs()));
    connect(ui->sum_of_bets, SIGNAL(textEdited(QString)), this, SLOT(updateInputs()));

    connect(ui->corrCoeff1, SIGNAL(textEdited(QString)), this, SLOT(updateCorrection()));
    connect(ui->corrCoeff2, SIGNAL(textEdited(QString)), this, SLOT(updateCorrection()));

    connect(ui->correctItButton, SIGNAL(clicked(bool)), this, SLOT(correctButtonClicked()));
}

void PluggWindow::setProfitValue(QLabel *profitLabel, float value)
{
    if (value > 0)
    {
        profitLabel->setStyleSheet(profitStyleSheets["good"]);
    }
    else if (value == 0)
    {
        profitLabel->setStyleSheet(profitStyleSheets["normal"]);
    }
    else
    {
        profitLabel->setStyleSheet(profitStyleSheets["bad"]);
    }
    profitLabel->setText(QString::number(value));
}

void PluggWindow::updateInputs()
{

    try
    {
        plugg.setCoeff(Plugg::inputs_t::LEFT, my_atof(ui->am1coeff->text().toStdString().c_str()));
    }
    catch (std::invalid_argument) { /* pass */ }
    try
    {
        plugg.setCoeff(Plugg::inputs_t::RIGHT, my_atof(ui->am2coeff->text().toStdString().c_str()));
    }
    catch (std::invalid_argument) { /* pass */ }

    if (!ui->am1coeff->hasFocus())
    {
        ui->am1coeff->setText(QString::number(plugg.getCoeff(Plugg::inputs_t::LEFT)).replace('.',','));
    }
    if (!ui->am2coeff->hasFocus())
    {
        ui->am2coeff->setText(QString::number(plugg.getCoeff(Plugg::inputs_t::RIGHT)).replace('.',','));
    }

    // bets read inputs

    if (ui->bet1->hasFocus())
    {
        plugg.setBet(Plugg::inputs_t::LEFT, my_atof(ui->bet1->text().toStdString().c_str()));
    }

    if (ui->bet2->hasFocus())
    {
        plugg.setBet(Plugg::inputs_t::RIGHT, my_atof(ui->bet2->text().toStdString().c_str()));
    }

    if (ui->sum_of_bets->hasFocus())
    {
        plugg.setBet(Plugg::inputs_t::TOTAL, my_atof(ui->sum_of_bets->text().toStdString().c_str()));
    }

    // bets write inputs

    if (!ui->bet1->hasFocus())
    {
        ui->bet1->setText(QString::number(plugg.getBet(Plugg::inputs_t::LEFT)).replace('.',','));
    }
    if (!ui->bet2->hasFocus())
    {
        ui->bet2->setText(QString::number(plugg.getBet(Plugg::inputs_t::RIGHT)).replace('.',','));
    }
    if (!ui->sum_of_bets->hasFocus())
    {
        ui->sum_of_bets->setText(QString::number(plugg.getBet(Plugg::inputs_t::TOTAL)).replace('.',','));
    }

    // update profit fields

    auto resPair = plugg.calculate_result();

    setProfitValue(ui->profit1, resPair.first);
    setProfitValue(ui->profit2, resPair.second);

#ifdef QT_DEBUG
    std::cout << plugg.getCurrentlyFixed() << " | " << plugg.getDistributive() << " | " << plugg << std::endl;
#endif

}

void PluggWindow::updateCorrection()
{
    // update correction fields

    float coeff1 = my_atof(ui->corrCoeff1->text().toStdString().c_str());
    float coeff2 = my_atof(ui->corrCoeff2->text().toStdString().c_str());

    auto resCorrection = plugg.rebase_calculations(coeff1, coeff2);

    ui->correction1->setText(QString::number(resCorrection.first));
    ui->correction2->setText(QString::number(resCorrection.second));
}

void PluggWindow::correctButtonClicked()
{
    // update correction fields

    float coeff1 = my_atof(ui->corrCoeff1->text().toStdString().c_str());
    float coeff2 = my_atof(ui->corrCoeff2->text().toStdString().c_str());

    // TODO: results have to be a class members
    auto resCorrection = plugg.rebase_calculations(coeff1, coeff2);
    auto profits = plugg.calculate_result();

    ui->correction1->setText(QString::number(resCorrection.first));
    ui->correction2->setText(QString::number(resCorrection.second));

    setProfitValue(ui->profit1, profits.first + (coeff1-1) * resCorrection.first - resCorrection.second);
    setProfitValue(ui->profit2, profits.second + (coeff2-1) * resCorrection.second - resCorrection.first);
}

void PluggWindow::roundingChanged(int index)
{
    plugg.setRounding(roundingValues[index]);
}

void PluggWindow::setFixed()
{
    if (ui->fixed1->isChecked())
    {
        plugg.setCurrentlyFixed(PluggRounded::inputs_t::LEFT);
        return;
    }
    if (ui->fixed2->isChecked())
    {
        plugg.setCurrentlyFixed(PluggRounded::inputs_t::RIGHT);
        return;
    }
    if (ui->fixed12->isChecked())
    {
        plugg.setCurrentlyFixed(PluggRounded::inputs_t::TOTAL);
        return;
    }
}

void PluggWindow::setDistributive()
{
    bool distr1Checked = ui->distr1->isChecked();
    bool distr2Checked = ui->distr2->isChecked();

    if (!(distr1Checked ^ distr2Checked) || !(distr1Checked ^ distr2Checked))
    {
#ifdef QT_DEBUG
        qDebug() << "both";
#endif
        plugg.setDistributive(PluggRounded::inputs_t::TOTAL);
        return;
    }
    if (distr1Checked)
    {
#ifdef QT_DEBUG
        qDebug() << "left";
#endif
        plugg.setDistributive(PluggRounded::inputs_t::LEFT);
        return;
    }
    if (distr2Checked)
    {
#ifdef QT_DEBUG
        qDebug() << "right";
#endif
        plugg.setDistributive(PluggRounded::inputs_t::RIGHT);
        return;
    }
}
