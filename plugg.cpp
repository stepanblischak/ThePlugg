#include "plugg.h"

#include <cstring>

const size_t standart_buffer_size = 256;

#define QT_DEBUG

#ifdef QT_DEBUG
#include <QDebug>
#endif

Plugg::Plugg(float coeff1, float bet1, float coeff2, float bet2) :
    left(coeff1, bet1), right(coeff2, bet2)
{
    // calculate total right after setting left and right
    evalTotal();
    // set currently fixed left by default
    setCurrentlyFixed(inputs_t::LEFT);
    // set distributive total by default
    setDistributive(inputs_t::TOTAL);
#ifdef QT_DEBUG
    qDebug() << left.getBet() << left.getCoeff()
             << right.getBet() << right.getCoeff();
#endif
}

Plugg::Plugg(const Arm &left, const Arm &right) :
    left(left), right(right)
{
    // calculate total right after setting left and right
    evalTotal();
    // set currently fixed left by default
    setCurrentlyFixed(inputs_t::LEFT);
    // set distributive total by default
    setDistributive(inputs_t::TOTAL);
#ifdef QT_DEBUG
    qDebug() << left.getBet() << left.getCoeff()
             << right.getBet() << right.getCoeff()
             << getProfit();
#endif
}

void Plugg::calculate_bets()
{
    switch(getCurrentlyFixed())
    {
    case inputs_t::LEFT:
        right.setValue(left.getValue() / ratio);
        evalTotal();
        break;
    case inputs_t::RIGHT:
        left.setValue(right.getValue() * ratio);
        evalTotal();
        break;
    case inputs_t::TOTAL:
        left.setValue(total.getValue() / (1 + 1 / ratio));
        right.setValue(total.getValue() / (1 + ratio));
        break;
    }
}

std::pair<float, float> Plugg::calculate_result()
{
    // calculate wins from two arms
    float winLeft, winRight;
    winLeft = (left.getCoeff() - 1) * left.getValue() - right.getValue();
    winRight = (right.getCoeff() - 1) * right.getValue() - left.getValue();

    // return it as a pair (first - left, second - right)
    return std::pair<float, float>(winLeft, winRight);
}

std::pair<float, float> Plugg::rebase_calculations()
{
    std::pair<float, float> result(0.0f, 0.0f);

    float leftBet = left.getValue();
    float rightBet = right.getValue();
    float coeffRatio = left.getCoeff() / right.getCoeff();
    if (rightBet / leftBet > coeffRatio)
    {
        if (coeffRatio > 1)
            result.first = rightBet / coeffRatio - leftBet;
        else
            result.first = rightBet * coeffRatio - leftBet;
        return result;
    }
    else
    {
        if (coeffRatio < 1)
            result.second = leftBet / coeffRatio - rightBet;
        else
            result.second = leftBet * coeffRatio - rightBet;
        return result;
    }
}

std::pair<float, float> Plugg::rebase_calculations(float coeff1, float coeff2)
{
    std::pair<float, float> result(0.0f, 0.0f);
    std::pair<float, float> profits = calculate_result();

    // p1 + (k1 - 1) * x - y = p2 + (k2 - 1) * y -x
    // p1 + k1 * x - x - y = 0
    // y = p1 + (k1 - 1 ) * x
    // x = (y - p1) / (k1 - 1)
    //
    // y = (p1 - p2 + k1 * x) / k2 = 0
    // x = (p2 - p1) / k1

    if (profits.second > profits.first)
    {
        result.first = (profits.second - profits.first) / coeff1;
        result.second = (profits.first - profits.second + coeff1 * result.first) / coeff2;
    }
    else
    {
        result.second = (profits.first - profits.second) / coeff2;
        result.first = (profits.second - profits.first + coeff2 * result.second) / coeff1;
    }
    return result;
}

float Plugg::getBet(inputs_t bet) const
{
    auto element = translation.find(bet);
    if (element == translation.end())
    {
        return 0.0f;
    }
    return element->second->getValue();
}

void Plugg::setBet(inputs_t bet, float val)
{
    auto element = translation.find(bet);
    if (element == translation.end())
    {
        return ;
    }
    element->second->setValue(val);
    if (element->second->isFixed())
    {
        calculate_bets();
    }
    else
    {
        linkInputs(bet);
    }
}

float Plugg::getCoeff(Plugg::inputs_t bet) const
{
    auto element = arms.find(bet);
    if (element == arms.end())
    {
        return 0.0f;
    }
    return element->second->getCoeff();
}

void Plugg::setCoeff(Plugg::inputs_t bet, float val)
{
    auto element = arms.find(bet);
    if (element == arms.end())
    {
        return ;
    }
    element->second->setCoeff(val);
    setDistributive(distributive);
    calculate_bets();
}

Plugg::inputs_t Plugg::getCurrentlyFixed() const
{
    return currentlyFixed;
}

void Plugg::setCurrentlyFixed(inputs_t value)
{
    // set previous to false
    translation[currentlyFixed]->setFixed(false);
    // set current to true
    translation[value]->setFixed(true);
    // set as current
    currentlyFixed = value;

    // recalculate
    calculate_bets();
}

float Plugg::getProfit() const
{
    return (1 / (1 / left.getCoeff() + 1 / right.getCoeff())) - 1;
}

void Plugg::linkInputs(inputs_t linkTo)
{
    // get currently fixed element
    // and fix other elements by it and linkTo element
    switch(linkTo)
    {
    case inputs_t::LEFT:
        linkLeft();
        break;
    case inputs_t::RIGHT:
        linkRight();
        break;
    case inputs_t::TOTAL:
        linkTotal();
        break;
    default:
        break;
    }
}

void Plugg::linkLeft()
{
    // link elements to fixed left arm
    switch(getCurrentlyFixed())
    {
    case inputs_t::RIGHT:
        evalTotal();
        break;
    case inputs_t::TOTAL:
        evalRight();
        break;
    default:
        break;
    }
}

void Plugg::linkRight()
{
    // link elements to fixed right arm
    switch(getCurrentlyFixed())
    {
    case inputs_t::LEFT:
        evalTotal();
        break;
    case inputs_t::TOTAL:
        evalLeft();
        break;
    default:
        break;
    }
}

void Plugg::linkTotal()
{
    // link elements to fixed total
    switch(getCurrentlyFixed())
    {
    case inputs_t::RIGHT:
        evalLeft();
        break;
    case inputs_t::LEFT:
        evalRight();
        break;
    default:
        break;
    }
}

void Plugg::evalLeft()
{
    // eval left arm due to total and right arms
    left.setValue(total.getValue() - right.getValue());
}

void Plugg::evalRight()
{
    // eval right due to total and left arms
    right.setValue(total.getValue() - left.getValue());
}

void Plugg::evalTotal()
{
    // eval total due to left and right arms
    total.setValue(left.getValue() + right.getValue());
}

Plugg::inputs_t Plugg::getDistributive() const
{
    return distributive;
}

void Plugg::setDistributive(inputs_t value)
{
    distributive = value;
    ratio = ratio_evaluators[value](*this);
#ifdef QT_DEBUG
    qDebug() << "RATIO:" << ratio;
#endif

    // recalculate
    calculate_bets();
}

std::ostream& operator<<(std::ostream &os, const Plugg &plugg)
{
    const char* representation = "Plugg (coeff=%f bet=%f) / (coeff=%f bet=%f) %f %%)";
    char buffer[standart_buffer_size];
    memset(buffer, '\0', standart_buffer_size);
    std::snprintf(buffer, standart_buffer_size,
                  representation,
                  plugg.left.getCoeff(),
                  plugg.left.getValue(),
                  plugg.right.getCoeff(),
                  plugg.right.getValue(),
                  plugg.getProfit());

    return os << buffer;
}
