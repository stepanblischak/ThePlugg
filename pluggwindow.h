#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>

#include <vector>
#include <map>

#include "pluggroundaddapter.h"

namespace Ui {
class PluggMainWindow;
}

// Application window class
class PluggWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PluggWindow(QWidget *parent = 0);
    ~PluggWindow();

private:
    // setups widgets, make signal-slot connections
    void setupWidgets();
    // helpful function to set proft value and background
    // color of the label depending on the value
    void setProfitValue(QLabel* profitLabel, float value);

public slots:
    // read all inputs -> do calculations -> write all results
    // into inputs
    void updateInputs();
    void updateCorrection();
    // correct button
    void correctButtonClicked();
    // slots for other parametr changing
    void roundingChanged(int index);
    void setFixed();
    void setDistributive();

private:
    // window title
    const QString windowName = "The Plugg";
    // vector of rounding values for combo box
    const std::vector<int> roundingValues =
    {
        1, 5, 10, 50, 100, 1000 // round bets to this precisions
    };
    std::map<const QString, const QString> profitStyleSheets =
    {
        {"good", "QLabel { background-color : green; color : black; }"},
        {"bad", "QLabel { background-color : red; color : black; }"},
        {"normal", "QLabel { color : black; }"}
    };
    Ui::PluggMainWindow *ui;
    PluggRounded plugg;
};

#endif // MAINWINDOW_H
