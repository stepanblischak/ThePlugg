#include "utils.h"

#include <sstream>
#include <locale>
#include <cstdlib>
#include <cstring>

const size_t standart_buffer_size = 256;

float my_atof(const char *number)
{
    float result = 0.0f;
    char buffer[standart_buffer_size];
    memset(buffer, char('\0'), standart_buffer_size*sizeof(char));
    strncpy(buffer, number, standart_buffer_size);

    int i = 0;
    while (buffer[i++] != '\0')
    {
        if (buffer[i] == char(','))
        {
            buffer[i] = char('.');
        }
    }

    std::istringstream istr(buffer);
    istr.imbue(std::locale("C"));
    istr >> result;

    return result;
}
