#ifndef FIXEDVALUE_H
#define FIXEDVALUE_H

// template structure FixedValue
// has a fixed flag to indicate for
// other that it needs to be fixed
// but it does not forbid changing
// incapsulating value even if the
// fixed flag is set.
template <typename T>
struct FixedValue
{
public:
    FixedValue() :
        value(0), fixed(false)
    {
    }
    FixedValue(const T &val, bool fixed) :
        value(val), fixed(fixed)
    {
    }
    FixedValue(const FixedValue &anotherFixedValue)
    {
        value = anotherFixedValue.value;
        fixed = anotherFixedValue.fixed;
    }

    bool isFixed() const
    {
        return fixed;
    }
    void setFixed(bool value)
    {
        fixed = value;
    }

    T getValue() const
    {
        return value;
    }
    void setValue(const T &val)
    {
        value = val;
    }

private:
    T value;
    bool fixed;
};

#endif // FIXEDVALUE_H
