#include "pluggroundaddapter.h"

#include <cmath>

#define ROUND_VALUE 0.5f

PluggRounded::PluggRounded(float coeff1, float bet1, float coeff2, float bet2, int rounding_) :
    Plugg(coeff1, bet1, coeff2, bet2)
{
    setRounding(rounding_);
}

PluggRounded::PluggRounded(const Arm &left, const Arm &right, int rounding_) :
    Plugg(left, right)
{
    setRounding(rounding_);
}

void PluggRounded::setBet(Plugg::inputs_t bet, float val)
{
    Plugg::setBet(bet, val);
    roundIt(bet);
    // recalculate unrounded total value
    Plugg::evalTotal();
}

void PluggRounded::setCoeff(Plugg::inputs_t bet, float val)
{
    Plugg::setCoeff(bet, val);
    roundIt();
    // recalculate unrounded total value
    Plugg::evalTotal();
}

void PluggRounded::roundIt(PluggRounded::inputs_t inputing)
{
    auto fixedElement = getCurrentlyFixed();
    for (auto element: translation)
    {
        auto inputs = element.first;
        auto value = element.second;
        if (inputs == fixedElement
                || inputs == inputing
                || inputs == PluggRounded::inputs_t::TOTAL)
        {
            continue;
        }

        value->setValue(floor((value->getValue() + ROUND_VALUE)/rounding)*rounding);
    }
}

void PluggRounded::roundIt()
{
    auto fixedElement = getCurrentlyFixed();
    for (auto element: translation)
    {
        auto inputs = element.first;
        auto value = element.second;
        if (inputs == fixedElement
                || inputs == PluggRounded::inputs_t::TOTAL)
        {
            continue;
        }

        value->setValue(floor((value->getValue() + ROUND_VALUE)/rounding)*rounding);
    }
}

int PluggRounded::getRounding() const
{
    return rounding;
}

void PluggRounded::setRounding(int value)
{
    rounding = value;
}
